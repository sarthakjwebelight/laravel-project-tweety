<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    use Likeable; // too many functions related to like feature in tweet model were quite tough so extracted them in trait

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
  
}
