<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use Notifiable, Followable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'username','name' ,'avatar' , 'email', 'password',
    ];
    // if we dont want to always include a field in fillable array 
    // and site is your personal use only we can
    //  protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getAvatarAttribute($value)
    {
           //'storage/' . added important
           // note here in 2nd value system is automatically guessing that /images... or 
           // any path is inside public dir only maybe this is due to link we provided in filesystems.php
        return asset($value ? 'storage/'. $value : '/images/default-avatar.png');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function timeline()
    {
        //include all of the user's tweets
        //as well as the tweets of everyone
        //they follow ... in descending order by date.
        $friends= $this->follows()->pluck('id');
     //   $ids->push($this->id);

         return Tweet::whereIn('user_id',$friends)
         ->orWhere('user_id', $this-> id)
         ->withLikes()
         ->latest()
         ->paginate(50);   
    }

    public function tweets()
    {
        // users own tweet  for his profile page logic
        return $this->hasMany(Tweet::class)->latest();
    }

    public function likes(){
        return $this->hasMany(Like::class);
    }
 

    public function path($append = '')
    {
        $path = route('profile' ,$this->username);

        return $append ? "{$path}/{$append}" : $path;
    }
       
}

