<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\Request;

class ExploreController extends Controller
{
    public function __invoke()
    {
        
        return view('explore' , [
            'users' => User::Paginate(50),

        ]);
      
    }   
}
