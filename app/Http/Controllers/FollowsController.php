<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class FollowsController extends Controller
{
    public function store(User $user)
    {
        // have the authenticated user follow the given user whos profile he she is viewing
       auth()->user()->toggleFollow($user);
    
       return back();
    }
}
