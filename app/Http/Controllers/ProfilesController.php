<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProfilesController extends Controller
{
   public function show(User $user)
   {
      return view('profiles.show',[
         'user' => $user,
         'tweets'=> $user->tweets()->withLikes()->paginate(50), 
      ]);
   }

   public function edit(User $user)
   {
      //1st method to autorize laravel provides abort if function  abort_if($user->isNot(current_user()), 404);
      //2nd method to authorize $this->authorize('edit', $user);
      //3rd method is through middleware in web.php file 
   
      return view('profiles.edit', compact('user'));
   }

   public function update(User $user)
   {
      
      $attributes =  request()->validate([
         'username' => ['string','required','max:255','alpha_dash',Rule::unique('users')->ignore($user)],
         'name' => ['string', 'required', 'max:255'],
         'avatar' => ['file'],
         'email' => ['string', 'required', 'email', 'max:255', Rule::unique('users')->ignore($user)],
         'password' => ['string', 'required', 'min:8', 'max:255', 'confirmed']
      ]);
     
      if (request('avatar'))
      {$attributes['avatar'] =  request('avatar')->store('avatar');}

      $user->update($attributes);

      return redirect($user->path());
   }
}
